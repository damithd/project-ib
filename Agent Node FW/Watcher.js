//create connection with server
var net = require('net');
var fs=require('fs');

var HOST = '169.254.251.99';
var PORT = 8080;
    
var client = new net.Socket();

client.connect(PORT, HOST, function(err) 
    {
        if(!err){
            var data= "The connection was made with the agent";
            console.log('CONNECTED TO: ' + HOST + ':' + PORT);

            //send data to server
            client.write(data);
        }
    }
);


//read url sent from server
client.on('data', function(data,err) 
    {
        console.log('Url: ' + data);
        var fs = require("fs");
        var StringDecoder = require('string_decoder').StringDecoder;
        var decoder = new StringDecoder('utf8');
        var ddata=decoder.write(data);
        
        //send url sent by server for watching
        fs.watch(ddata, { persistent: true }, function (event, fileName) 
            {
                console.log("started watching"+data);  
                var path= data+fileName;
                console.log("Event: " + event + path );
                console.log(fileName + "\n");
                var changedFileSize = readFileSize(path);
                readfile(fileName,changedFileSize);  
                updateFileSize(changedFileSize,fileName);
            }
        );
        client.destroy();
    }
);

// Add a 'close' event handler for the client socket
client.on('close', function(err) 
    {
        if(err){
            console.log('Connection closed');
        }
    }
);
    





//Read size of the log file
function readFileSize(path){
    var fs = require("fs"); 
    var stats = fs.statSync(path);
    var fileSizeInBytes = stats["size"];
    console.log('file size =' + fileSizeInBytes);
    return fileSizeInBytes;
}
 
 



//Update changed size of log file in database 
function updateFileSize(changedFileSize,fileName){
        var mysql = require('mysql');

        var connection = mysql.createConnection(
            {
              host     : 'localhost',
              user     : 'root',
              password : '',
              database : 'clientdb',
            }
        );

        connection.connect();

        connection.query("UPDATE FILE SET size = " + mysql.escape(changedFileSize) +" where name="+ mysql.escape(fileName) , function(err,rows,fields)              {
                if (err) throw err;
                console.log("record updated");
             }
        );

        var queryString = 'SELECT * FROM file';
        connection.query(queryString, function(err, rows, fields) 
            {
                if (err) throw err;
                for (var i in rows) {
                    console.log('Changed file size ', rows[i].size);}
                    var prevSize=rows[i].size;
                    console.log(prevSize);
            }
        );

        connection.end();
}


 
 
 
//Read the added content to the log file  
function readfile(fileName,changedFileSize){
    
    //create database connection
    var mysql = require('mysql');
    var connection = mysql.createConnection(
        {
          host     : 'localhost',
          user     : 'root',
          password : '',
          database : 'clientdb',
        }
    );

    var queryString = "SELECT size FROM file where name="+ mysql.escape(fileName); 
    
    //Retrieve the previous size of log file from db
    connection.query(queryString, function(err, rows, fields) 
        {
            if (err) throw err;
            for (var i in rows) {
                console.log('Changed file size ', rows[i].size);
            }
            var prevSize=rows[i].size;
            console.log(prevSize);
            connection.end();

            //Read the added content of the log file 
            var fs = require("fs");
            fs.exists(fileName, function (exists) 
                {
                    if (exists) {
                        fs.stat(fileName, function (error, stats) 
                            {
                                fs.open(fileName, "r", function (error, fd) 
                                    {
                                        var buffer = new Buffer(stats.size-prevSize ); 
                                        console.log(prevSize+"fs");
                                        fs.read(fd, buffer, 0, buffer.length, prevSize, function (error, bytesRead, buffer)
                                            {
                                                var data = buffer.toString("utf8", 0, buffer.length);
                                                console.log(data+"\n");
                                                console.log(data);

                                                //Create connection with server
                                                var net = require('net');
                                                var HOST = '169.254.251.99';
                                                var PORT = 8080;
                                                var client = new net.Socket();
                                                client.connect(PORT, HOST);
                                                console.log('CONNECTED TO: ' + HOST + ':' + PORT);
                                                console.log(data+"*");
                                                
                                                //send read data to server
                                                client.write(data);
                                                console.log("\n data sent to server");
                                                          
                                            }
                                        );
                                    }
                                );
                            } 
                        );
                    }                  
                }
             );                                              
         }
    );
}