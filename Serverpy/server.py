from pytransmit import FTPClient
import socket
from functools import wraps
from flask import Flask 
from flask import Flask ,url_for,redirect,session,abort,flash
from flask import request
from flask import render_template
#from flask.ext.wtf import Form 
#from wtforms import StringField, SubmitField ,IntegerField
#from app import app
#from flask_bootstrap import Bootstrap
import os
from sqlalchemy.orm import sessionmaker
from tabledef import *
import datetime
from sqlalchemy import create_engine
#from sqlalchemy.orm import sessionmaker
#from tabledef import *
engine = create_engine('sqlite:///tutorial.db', echo=True)
ftp_obj=FTPClient()
app = Flask(__name__)


######################### Index Page #############################
@app.route('/')
def index(): 
    return render_template("index.html")
    
    
    
############################ Login ###############################
@app.route('/userLogin.html',methods=['GET', 'POST']) 
def home():
    if not session.get('logged_in'):
        return render_template("userLogin.html")
        return redirect('/')
    
    else:
        return redirect("profile.html")

    
############################# SignUp #############################
@app.route('/userSignup.html',methods=['GET', 'POST']) 
def home1():
    if not session.get('logged_in'):
        return render_template("userSignup.html")
       
    else:
        return redirect('/')

    
######################## Login_controller ########################
@app.route('/login',methods=['GET', 'POST']) 
def LogIn():
    
    
    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])

    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]) )
    result = query.first()
    if result:
        session['logged_in'] = True
        
    else:
        flash('wrong password!')
    
    return home()

 
######################## SignUp_controller #########################
@app.route('/signup',methods=['GET','POST'])
def SignUp():
    #return render_template('exmp_signup.html')
    un = request.form['username']
    pw = request.form['password']
    
    Session = sessionmaker(bind=engine)
    s = Session() 
    query = s.query(User).filter(User.username.in_([un]), User.password.in_([pw]) )
    result = query.first()
    if result:
        session['logged_in'] = True
        return redirect('existingAccount.html')
        
    else:
        me=User(un,pw)
        s.add(me)
        s.commit()
        return redirect('/')
    

########################## Account_exists ###########################
@app.route('/existingAccount.html')
def accnt_exst():
    session['logged_in'] = False
    return render_template('existingAccount.html')
        
    return home1()


########################### User_profile ############################
@app.route('/profile.html',methods=['GET','POST'])
def profile():
    return render_template("profile.html")
    return redirect('createProject.html')
   

######################## Create a new project ########################
@app.route('/createProject.html',methods=['GET','POST'])
def profile_final():
    return render_template("createProject.html")
    return redirect('showChange.html')


##################### Show change sent by client #####################
@app.route('/showChange.html',methods=['GET','POST'])
def profile_nxt(): 
    return render_template("showChange.html")
    

#################### Create directory for project ####################
@app.route('/enterUrl', methods=['GET','POST'])
def enterUrl():
    projectName = request.form['projectName']
    url = request.form['url'] 
    
    print("Project name is "+projectName+"  The url to watch is " + url + "" )
   
    ftp_obj=FTPClient()
    # FTP Details
    FTP_HOST = "10.128.0.81"
    FTP_USER = ""
    FTP_PASS = ""
    FTP_PORT = 21
    
    # Connect to FTP server
    ftp_obj.connect(FTP_HOST, FTP_USER, FTP_PASS, FTP_PORT)
    print(ftp_obj.get_message())
    path='\workspace'
    widgets='\widgets'
    projectName1='\\'+projectName
    
    # Create a new directory for the project
    directory = path
    ftp_obj.change_directory(directory)
    directory='\workspace\\'+projectName
    ftp_obj.make_directory(directory)
    ftp_obj.change_directory(directory)
    
    directory='\workspace\\'+projectName+'\widgets'
    ftp_obj.make_directory(directory)
    print(ftp_obj.get_message())
    app_root = os.path.dirname(os.path.abspath(__file__))
    
    #change directory
    directory='\\'
    ftp_obj.change_directory(directory)
    directory='\data'
    ftp_obj.change_directory(directory)
    print(ftp_obj.get_message())
       
    
    # read file
    with open(os.path.join(app_root, 'projects.php'),'rb+') as fread:
        fread.seek(-6,2)
        fread.truncate()
        data=fread.read()
        print(data)
    
    # apppend added project name 
    with open(os.path.join(app_root, 'projects.php'),'a') as fappend:
        appendData=",{\"name\":"+"\""+projectName+"\""+",\"path\":"+"\""+projectName+"\""+"}]|*/?>"   
        fappend.write(appendData)
        print(ftp_obj.get_message())
        
    # Upload the edited file
    directory='\data'
    ftp_obj.change_directory(directory)
    print(ftp_obj.get_message())
    file_path = os.path.join(app_root, 'projects.php')
    strip_path = file_path.rstrip(os.sep)
    fp_path = os.path.basename(strip_path)

    # Upload command
    ftp_obj.upload_file(fp_path)
    print(ftp_obj.get_message()) 
    
    session['projectName']=projectName
    session['url']=url 
    
    return redirect('/sendUrl')


######################## Send url to client ###########################
@app.route('/sendUrl',methods=['GET', 'POST'])
def sendUrl(): 
        projectName=session['projectName']
        url=session['url']
        
        # create connection with client
        TCP_IP = '0.0.0.0'
        TCP_PORT = 8080
        BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
        #print("The email address is '" + email + "dilshan")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((TCP_IP, TCP_PORT))
        s.listen(1)
   
        conn, addr = s.accept()
        print 'Connection address:', addr
                   
        data = conn.recv(BUFFER_SIZE)
        #connection established
        print "received data:", data
        
        #send url to client
        conn.send(url.encode())
        conn.close()
        print("done")
        return redirect('/index_test.html')   

    
##################### Recieve change sent by client ########################
@app.route('/index_test.html',methods=['GET', 'POST']) 
def profile_next():    
    
    projectName=session['projectName']
    #create connection with client
    TCP_IP = '0.0.0.0'
    TCP_PORT = 8080
    BUFFER_SIZE = 1024
    
    
    s = socket.socket()         # Create a socket object
    s.bind((TCP_IP, TCP_PORT))        # Bind to the port
    s.listen(5)                 # Now wait for client connection.
    while True:
        c, addr = s.accept()     # Establish connection with client.
        print 'Got connection from', addr
        
        change=c.recv(1024)
        print change
        user = {'change': change} 
        c.close()  # Close the connection
        
        # FTP Details
        FTP_HOST = "10.128.0.81"
        FTP_USER = ""
        FTP_PASS = ""
        FTP_PORT = 21

        # Connect to the server
        ftp_obj.connect(FTP_HOST, FTP_USER, FTP_PASS, FTP_PORT)
        print(ftp_obj.get_message())
        app_root = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(app_root, 'log.txt'),'w') as f:
            f.write(change)
            f.close()
        directory='\workspace'
        ftp_obj.change_directory(directory)
        directory='\workspace\\'+projectName
        ftp_obj.change_directory(directory)
        file_path = os.path.join(app_root, 'log.txt')
        strip_path = file_path.rstrip(os.sep)
        fp_path = os.path.basename(strip_path)

        # Upload command
        ftp_obj.upload_file(fp_path)
        file_path = os.path.join(app_root, 'script.ldel')
        strip_path = file_path.rstrip(os.sep)
        fp_path = os.path.basename(strip_path)

        # Upload command
        ftp_obj.upload_file(fp_path)
        
        file_path = os.path.join(app_root, 'defs.config')
        strip_path = file_path.rstrip(os.sep)
        fp_path = os.path.basename(strip_path)

        # Upload command
        ftp_obj.upload_file(fp_path)
        
        print(ftp_obj.get_message())   
        return render_template("index_test.html",user=user)



############################### Logout ##############################
@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

if __name__ == '__main__':
    app.secret_key = os.urandom(12)
    app.run(debug=True,host='0.0.0.0', port=5001)